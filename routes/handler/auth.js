const adminsModel = require("../../models/admins_model");
const bcrypt = require("bcrypt");

function Auth() {
  this.index = async (req, res) => {
    if (req.session?.user) {
      return res.redirect("/");
    }
    return res.render("pages/signin", {
      title: "Signin License Manager",
    });
  };

  this.signup = async (req, res) => {
    if (req.session?.user) {
      return res.redirect("/");
    }
    return res.render("pages/signup", {
      title: "Signup License Manager",
    });
  };

  this.validateAuth = async (req, res) => {
    let { email, password } = req.body;
    await adminsModel
      .findOne({ $or: [{ email }, { username: email }] })
      .then(async (user) => {
        if (!user) {
          return res.json({
            status: 404,
            message: "User tidak ditemukan",
          });
        }

        if (!user.aktif) {
          return res.json({
            status: 403,
            message: "Status User tidak aktif",
          });
        }

        const isPasswordMatch = await bcrypt.compare(password, user.password);
        if (!isPasswordMatch) {
          return res.json({
            status: 400,
            message: "Password tidak sesuai",
          });
        }

        req.session.user = {
          id: user._id,
          email: user.email,
          aktif: user.aktif,
        };

        return res.json({
          status: 200,
          message: `Login Success`,
        });
      })
      .catch((error) => {
        console.log("Error : ", error);
        res.json({
          status: 500,
          message: "Internal server error !",
        });
      });
  };

  this.checkAuth = async (req, res, next) => {
    if (req.session?.user) {
      adminsModel
        .findOne({
          _id: req.session.user.id,
        })
        .then(async (user) => {
          if (!user) return res.redirect("/signin");
          return next();
        });
    } else {
      return res.redirect("/signin");
    }
  };

  this.logout = async (req, res) => {
    req.session.destroy();
    res.redirect("/signin");
  };
}

module.exports = exports = Auth;
