const licensesModel = require("../../models/licenses_model");
const crypto = require("crypto");

function License() {
  this.index = async (req, res) => {
    return res.render("pages/license", {
      title: "Manage Lisensi",
    });
  };

  this.detail = async (req, res) => {
    try {
      const { id } = req.params;
      const data = await licensesModel.findOne({ _id: id });
      if (!data)
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });

      return res.json({
        status: "success",
        data,
      });
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.dataTable = async (req, res) => {
    try {
      const { draw, start, length, search } = req.query;
      const querySearch =
        search.value !== ""
          ? {
              $or: [
                {
                  deviceKey: { $regex: `.*${search.value}.*` },
                },
                {
                  type: { $regex: `.*${search.value}.*` },
                },
              ],
            }
          : {};

      const data = await licensesModel
        .find()
        .sort({ _id: -1 })
        .skip(parseInt(start))
        .limit(parseInt(length))
        .where({ ...querySearch });

      const totalCount = await licensesModel
        .countDocuments()
        .where({ ...querySearch });

      const dataTable = {
        draw,
        recordsTotal: totalCount,
        recordsFiltered: totalCount,
        data,
      };
      return res.json(dataTable);
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.createLicense = async (req, res) => {
    try {
      let { deviceKey, status, type } = req.body;
      if (!deviceKey || !type || !status)
        return res.status(400).json({
          status: "error",
          message: "Data belum lengkap",
        });

      const license = new licensesModel({
        deviceKey: crypto.createHash("sha256").update(deviceKey).digest("hex"),
        type,
        isActive: status,
      });

      await license.save();

      return res.status(201).json({
        status: "success",
        message: "Data lisensi berhasil ditambahkan",
      });
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.updateLicense = async (req, res) => {
    try {
      let { id, deviceKey, type, status } = req.body;
      if (!id)
        return res.status(400).json({
          status: "error",
          message: "Invalid id",
        });

      const license = await licensesModel.findOne({ _id: id });
      if (!license)
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });

      deviceKey =
        deviceKey === license.deviceKey
          ? license.deviceKey
          : crypto.createHash("sha256").update(deviceKey).digest("hex");

      await licensesModel.findOneAndUpdate(
        { _id: id },
        { deviceKey, type, isActive: status }
      );
      return res.json({
        status: "success",
        message: "Data berhasil diperbarui",
      });
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.deleteLicense = async (req, res) => {
    try {
      if (!req.body.id)
        return res.status(400).json({
          status: "error",
          message: "Invalid license id",
        });

      const license = await licensesModel.findOne({ _id: req.body.id });
      if (!license)
        return res.status(404).json({
          status: "error",
          message: "Data tidak ditemukan",
        });

      await licensesModel.findOneAndRemove({ _id: req.body.id });
      return res.json({
        status: "success",
        message: "Data berhasil dihapus",
      });
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };

  this.validate = async (req, res) => {
    try {
      const { key, type } = req.query;

      const license = await licensesModel.findOne({
        type,
        deviceKey: crypto.createHash("sha256").update(key).digest("hex"),
      });

      if (!license)
        return res.status(400).json({
          status: "error",
          access: false,
          message: "Mohon maaf, device tidak terdaftar",
        });

      if (!license.isActive)
        return res.status(400).json({
          status: "error",
          access: false,
          message: "Mohon maaf, lisensi anda sudah tidak aktif",
        });

      return res.json({
        status: "success",
        access: true,
      });
    } catch (error) {
      console.log("[!] error : ", error);
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  };
}

module.exports = exports = License;
