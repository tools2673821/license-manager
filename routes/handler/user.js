const adminsModel = require("../../models/admins_model");

const bcrypt = require("bcrypt");

function User() {
  this.create = async (req, res) => {
    const source = req.body;
    try {
      const { name, username, email, password } = source;
      if (!name || !email || !password || !username) {
        return res.json({
          status: 400,
          message: "Data belum lengkap",
        });
      }

      const payload = {
        name,
        email,
        username,
        password: bcrypt.hashSync(source.password, 10),
        aktif: source.akfit ? source.akfit : true,
      };

      const user = new adminsModel(payload);
      await user.save();

      return res.json({
        status: 201,
        message: "Signup Success",
      });
    } catch (err) {
      console.log("Error : ", err);
      if (err && err.name === "ValidationError") {
        return res.json({
          status: 433,
          message: err.message,
        });
      }

      // validate unique field
      if (err && err.name === "MongoServerError") {
        let value = "";
        for (let key in err.keyValue) {
          value = err.keyValue[key];
        }
        return res.json({
          status: 433,
          message: `Users validation failed: ${Object.keys(
            err.keyValue
          )} : ${value} sudah terdaftar`,
        });
      }
    }
  };
}

module.exports = exports = User;
