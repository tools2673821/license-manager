const router = require("express").Router();
const AuthHandler = require("./handler/auth"),
  Auth = new AuthHandler();
const LicenceHandler = require("./handler/license"),
  Licence = new LicenceHandler();
const UserHandler = require("./handler/user"),
  User = new UserHandler();

router.get("/", Auth.checkAuth, (req, res) => {
  res.render("index", { title: "Payuni" });
});

//  Auth
router.get("/signin", Auth.index);
// router.get("/signup", Auth.signup);
router.get("/signout", Auth.logout);
router.post("/validateAuth", Auth.validateAuth);

// Users
router.post("/user/create", User.create);

// API VALIDATE DEVICE KEY
router.get("/validate", Licence.validate);

// MANAGE LICENCES
router.use("/manage-license", Auth.checkAuth);
router.get("/manage-license", Licence.index);
router.get("/manage-license/detail/:id", Licence.detail);
router.get("/manage-license/datatable", Licence.dataTable);
router.post("/manage-license/create", Licence.createLicense);
router.post("/manage-license/update", Licence.updateLicense);
router.delete("/manage-license/delete", Licence.deleteLicense);

module.exports = router;
