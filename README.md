# Payuni License Manager


# Installation and Configuration

### Configuration

- setting up database configuration in /config/db.json

### Running using docker-compose

```bash
$ git clone git@gitlab.com:tools2673821/license-manager.git
$ cd license-manager
$ docker-compose up -d --build
```

## Credits
Built with Nodejs v16


## API validation license
  /valudate?key0xxx1xxx&type=addon

---

Copyright ©️ 2023 by PT. Fintek Digital Nusantara