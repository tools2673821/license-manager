const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminsModel = new Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: [true, "Username harus diisi"],
    unique: [true, "Username sudah digunakna"],
  },
  email: {
    type: String,
    required: [true, "Email harus diisi"],
    unique: [true, "Email sudah digunakna"],
  },
  password: {
    type: String,
    required: [true, "Password harus diisi"],
  },
  aktif: {
    type: Boolean,
  },
});

module.exports = exports = mongoose.model("adminsModel", adminsModel, "admins");
