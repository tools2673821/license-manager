const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const licensesModel = new Schema(
  {
    deviceKey: {
      type: String,
      required: true,
      unique: true,
    },
    isActive: {
      type: Boolean,
      defalt: true,
    },
    type: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = exports = mongoose.model(
  "licensesModel",
  licensesModel,
  "licenses"
);
