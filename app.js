require("dotenv").config();
const createError = require("http-errors");
const express = require("express");
const app = express();
const cors = require("cors");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mode = process.env.NODE_ENV;
const configDB = require("./config/db.json")[mode];
const configRedis = require("./config/redis.json")[mode];
const port = process.env.PORT;
const path = require("path");
const mongoose = require("mongoose");
const indexRouter = require("./routes/index");
const session = require("express-session");
const { createClient } = require("redis");
const connectRedis = require("connect-redis")(session);

// VIEW ENGINE
app.set("view engine", "pug");
app.set("views", path.join("views"));

// MIDDLEWARE
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

let authMongoDB =
  configDB.password != ""
    ? configDB.username + ":" + configDB.password + "@"
    : "";
mongoose
  .connect(
    `mongodb://${authMongoDB}${configDB.host}:${configDB.port}/${configDB.db}`,
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    }
  )
  .then((connected) => {
    console.log("[!] MONGODB CONNECTED ON HOST : " + configDB.host);
    // setup redis
    const url = `redis://${configRedis.host}:${configRedis.port}`;
    let client = createClient({ url, legacyMode: true });
    client.connect().catch(console.error);
    client.on("connect", () =>
      console.log(
        `[!] Redis Connect on Host ${configRedis.host}:${configRedis.port}`
      )
    );

    app.use(
      session({
        secret: configRedis.secret,
        name: configRedis.sessioName,
        resave: true,
        saveUninitialized: false,
        cookie: {
          secure: false,
          maxAge: 21600000,
        },
        store: new connectRedis({
          host: configRedis.host,
          port: configRedis.port,
          db: configRedis.db,
          client,
          ttl: 260,
        }),
      })
    );
    
    app.use("/", indexRouter);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
      next(createError(404));
    });

    // error handler
    app.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render("error");
    });

    app.listen(port, () =>
      console.log(`[!] Service running on port : ${port}`)
    );
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
